# README #

This repository has been stood up to host the python methods needed to:

* extract the text contents of various file formats contracts are saved in
* cleanup or do any pre-processing of the ingested file formats

### What is this repository for? ###

* All the methods needed for preprocessing of contracts needed for AGS project


### How do I get set up? ###

* pip install -r requirements.txt (todo: add requirements.txt)
* import the method needed. eg. from documents_to_text import extract_pdf


### Who do I talk to? ###

* Pratik Baral
* Anne Regel